import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  componentMode: string;
  listBooks: any;
  currentBook: any;
  currentPage: number = 0;
  size: number = 5;
  totalPages: number;
  pages: number[];

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.componentMode = "all-books-mode";
    this.httpClient.get(`http://localhost:8080/api/visitor/books?page=${this.currentPage}&size=${this.size}`)
      .subscribe((data: any) => {
        this.totalPages = data.totalPages;
        this.pages = new Array<number>(this.totalPages);
        this.listBooks = data;
        console.log(data);
      }, error => {
        console.log(error);
      })
  }

  moreInfo(book) {
    this.componentMode = "one-book-mode";
    this.currentBook = book;
  }

  allBooks() {
    this.componentMode = "all-books-mode";
  }

  getBooks(category: string) {
    this.componentMode = "all-books-mode";
    this.currentPage = 0;
    if(category === "All")
      this.ngOnInit();
    else
      this.httpClient.get("http://localhost:8080/api/visitor/books/byCat?catName="+category)
        .subscribe((data: any) => {
          this.listBooks = data;
          this.totalPages = data.totalPages;
          this.pages = new Array<number>(this.totalPages);
        }, error => {
          console.log(error);
        })
  }

  getBooksPage(i: number) {
    this.currentPage = i;
    this.ngOnInit();
  }

  getAdjacentPage(next: number) {
    if(!((this.currentPage + 1 === this.totalPages && next === 1) || (this.currentPage === 0 && next === -1))){
      this.currentPage += next;
      this.ngOnInit();
    }
  }

  onSearchBook(formData: any) {
    this.componentMode = "all-books-mode";
    this.currentPage = 0;
    this.httpClient.get(`http://localhost:8080/api/visitor/searchBooks?key=${formData.key}&page=${this.currentPage}&size=${this.size}`)
      .subscribe((data: any) => {
        this.totalPages = data.totalPages;
        this.pages = new Array<number>(this.totalPages);
        this.listBooks = data;
        console.log(data);
      }, error => {
        console.log(error);
      })
  }
}
