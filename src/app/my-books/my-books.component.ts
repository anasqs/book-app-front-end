import {Component, HostListener, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthenticationService} from '../authentication.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-my-books',
  templateUrl: './my-books.component.html',
  styleUrls: ['./my-books.component.css']
})
export class MyBooksComponent implements OnInit {

  componentMode: string;
  listBooks: any;
  currentBook: any;
  fileToUpload: File = null;
  modifyMode: boolean;
  currentPage: number = 0;
  size: number = 3;
  totalPages: number;
  pages: number[];

  constructor(private httpClient: HttpClient) { }


  ngOnInit(): void {
    this.componentMode = "all-books-mode";
    this.modifyMode = false;
    const headersObject = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('authToken'));
    const httpOptions = { headers: headersObject};
    this.httpClient.get(`http://localhost:8080/api/user/myBooks?page=${this.currentPage}&size=${this.size}`, httpOptions)
      .subscribe((data:any) => {
        this.listBooks = data;
        this.totalPages = data.totalPages;
        this.pages = new Array<number>(this.totalPages);
        console.log(data);
        console.log(this.pages);
      }, error => {
        console.log(error);
      })
  }

  moreInfo(book) {
    this.componentMode = "one-book-mode";
    this.modifyMode = false;
    this.currentBook = book;
  }

  allBooks() {
    this.ngOnInit();
  }

  getBooks(category: string) {
    this.componentMode = "all-books-mode";
    if(category === "All")
      this.ngOnInit();
    else
    {
      const headersObject = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('authToken'));
      const httpOptions = { headers: headersObject};
      this.httpClient.get("http://localhost:8080/api/user/myBooksByCat?catName="+category, httpOptions)
        .subscribe(data => {
          this.listBooks = data;
        }, error => {
          console.log(error);
        })
    }
  }


  onChangeImage(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log("uploading "+ this.fileToUpload.name);
    this.postFile(this.fileToUpload)
      .subscribe(response => {
        console.log(response);
      }, error => {
        console.log(error);
      })
  }

  postFile(fileToUpload: File) {
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    formData.append('bookId', this.currentBook.id);
    const headersObject = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('authToken'));
    const httpOptions = { headers: headersObject};
    return this.httpClient.post("http://localhost:8080/api/user/addImagetoBook", formData, httpOptions)
  }

  onDeleteBook() {
    if(confirm("Are you sure you want to delete \""+this.currentBook.title+"\" ?"))
    {
      const headersObject = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('authToken'));
      const httpOptions = { headers: headersObject};
      this.httpClient.delete(`http://localhost:8080/api/user/${this.currentBook.id}`, httpOptions)
        .subscribe(response => {
          console.log(response);
          this.ngOnInit();
        }, error => {
          console.log(error);
        })
    }
  }

  onEdit() {
    this.modifyMode = true;
  }

  onChangeAvailability() {
    const headersObject = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('authToken'));
    const httpOptions = { headers: headersObject};
    this.httpClient.post(`http://localhost:8080/api/user/setBookSold/${this.currentBook.id}`, null, httpOptions)
      .subscribe(response => {
        console.log(response);
      }, error => {
        console.log(error);
      })
  }

  onAddNewBook() {
    this.componentMode = 'add-new-book-mode';
  }

  onSubmitBook(data: any) {
    const headersObject = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('authToken'));
    const httpOptions = { headers: headersObject};
    this.httpClient.post("http://localhost:8080/api/user/books", data, httpOptions)
      .subscribe(response => {
        console.log(response);
        this.currentBook = response;
        this.componentMode = 'one-book-mode';
      }, error => {
        console.log(error);
      })
  }

  getBooksPage(i: number) {
    this.currentPage = i;
    this.ngOnInit();
  }

  getAdjacentPage(next: number) {
    if(!((this.currentPage + 1 === this.totalPages && next === 1) || (this.currentPage === 0 && next === -1))){
      this.currentPage += next;
      this.ngOnInit();
    }
  }
}
