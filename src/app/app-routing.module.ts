import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BooksComponent} from './books/books.component';
import {MyBooksComponent} from './my-books/my-books.component';
import {LoginComponent} from './login/login.component';

const routes: Routes = [
  { path: '',   redirectTo: '/books', pathMatch: 'full' },
  { path: 'books', component: BooksComponent},
  { path: 'myBooks', component: MyBooksComponent},
  { path: 'login/:key', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
