import { Component } from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Book App';

  constructor(private authService: AuthenticationService,
              private router: Router) {
  }

  isAuthenticated() {
    return this.authService.isAuthneticated();
  }

  onLogOut() {
    this.authService.logout();
    this.router.navigate(['/books']);
  }

  authName() {
    return this.authService.authName();
  }
}
