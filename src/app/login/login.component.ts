import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../authentication.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isError: boolean;
  key: string;

  constructor(private authService: AuthenticationService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.isError = false;
    this.route.params
      .subscribe(params => {
        this.key = params.key;
      })
  }

  onLogin(data: any) {
    this.authService.login(data)
      .subscribe(response => {
        const jwt = response.headers.get('Authorization');
        this.authService.saveToken(jwt);
        this.router.navigate(['/books']);
      }, error => {
        console.log(error);
        this.isError = true;
      })
  }

  isAuthenticated() {
    return this.authService.isAuthneticated();
  }

  onRegister(data: any) {
    console.log(data);
    if(data.password !== data.confirmPassword)
      this.isError = true;
    else {
      this.authService.register(data)
        .subscribe(response => {
          console.log(response);
          this.isError = false;
          this.key = 'signin';
        }, error => {
          console.log(error);
        })
    }
  }
}
