import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  jwt: string;
  username: string;
  roles: [string];

  constructor(private httpClient: HttpClient) { }

  login(data: any) {
    console.log(data);
    return this.httpClient.post("http://localhost:8080/login", data, {observe: 'response'});
  }

  saveToken(jwt: string) {
    this.jwt = jwt;
    localStorage.setItem('authToken', jwt);
    this.parseJWT();
  }

  parseJWT() {
    const jwtHelper = new JwtHelperService();
    const decodedToken = jwtHelper.decodeToken(this.jwt);
    this.username = decodedToken.sub;
    this.roles = decodedToken.roles;
  }

  isAuthneticated() {
    if(localStorage.getItem('authToken') == null)
      return false;
    this.jwt = localStorage.getItem('authToken');
    this.parseJWT();
    return this.username != null;
  }

  logout() {
    localStorage.clear();
    this.username = '';
    this.jwt = '';
  }

  authName() {
    this.parseJWT();
    return this.username;
  }

  register(data: any) {
    return this.httpClient.post("http://localhost:8080/register", data);
  }
}
